let Flovour = {
        dev: {
                BASE_URL: 'http://13.71.26.73:4000/',
                ENABLE_DEBUG: true,
        },
        qa: {
                BASE_URL: 'http://104.211.103.115/',
                ENABLE_DEBUG: false,
        },
        preprod: {
                BASE_URL: 'http://test.srk.one/',
                ENABLE_DEBUG: false,
        }

}

export { Flovour };