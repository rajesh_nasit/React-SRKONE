import { Flovour } from './flavour';


const CONFIG = Flovour.dev;

 function writeLog(tag, message) {
    if (CONFIG.ENABLE_DEBUG)
        console.debug(tag, message);
}

export { CONFIG,writeLog };