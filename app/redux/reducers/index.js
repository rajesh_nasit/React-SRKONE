// reducers/index.js

import { combineReducers } from 'redux'
import deviceMatrixReducer from './deviceMatrixReducer'
import sessionReducer from './sessionReducer'




const rootReducer = combineReducers({
    deviceMatrixReducer,
    sessionReducer
})

export default rootReducer