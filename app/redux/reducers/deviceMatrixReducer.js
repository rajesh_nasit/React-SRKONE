// reducers/people.js
import { SET_DEVICEMATRIX_VALUE } from '../constants';

const initialState = { deviceMatrix:"" }

export default function deviceMatrixReducer(state = initialState, action) {

  switch (action.type) {
    case SET_DEVICEMATRIX_VALUE:
     
      return { ...state, deviceMatrix: action.deviceMatrix };

    default:
      return state;
  }
}
