// reducers/people.js
import { SET_SESSION_VALUE } from '../constants';

const initialState = { session:"" }

export default function sessionReducer(state = initialState, action) {

  switch (action.type) {
    case SET_SESSION_VALUE:
     
      return { ...state, session: action.session };

    default:
      return state;
  }
}
