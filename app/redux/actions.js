import { SET_DEVICEMATRIX_VALUE,SET_SESSION_VALUE} from './constants';

export function setDeviceMatrixValue(deviceMatrix) {
 
  return {
    type: SET_DEVICEMATRIX_VALUE,
    deviceMatrix
  };
}

export function setSessionValue(session) {
 
  return {
    type: SET_SESSION_VALUE,
    session
  };
}
