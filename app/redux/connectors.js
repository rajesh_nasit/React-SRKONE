import { connect } from 'react-redux';
import { setDeviceMatrixValue,setSessionValue } from './actions';
const helpers = {

  connect: function (componant) {
    const mapDispatchToProps = {
      setDeviceMatrixValue,
      setSessionValue
      
    };
    function mapStateToProps(state) {
      return {
        deviceMatrix: state.deviceMatrixReducer.deviceMatrix,
        session: state.sessionReducer.session
      }
    }
    return connect(
      mapStateToProps,
      mapDispatchToProps,
    )(componant)

    
  }
}


export default helpers;