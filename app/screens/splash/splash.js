import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, TouchableHighlight
} from 'react-native';

import { CONFIG } from '../../config/config';
import helpers from '../../redux/connectors';
import DeviceInfo from 'react-native-device-info';
import I18n from '../../config/locales/i18n';

import OfflineNotice from '../../lib/corelib/uiUtills/offlineNotice';

import * as ApiUtils from '../../lib/corelib/apiUtills/serviceManager';
import * as StaticUtils from '../../config/URLs'
import ProgressBar from '../../lib/corelib/uiUtills/progressBar';
import { setJSExceptionHandler } from 'react-native-exception-handler';
import errorAlert from '../../lib/corelib/exceptionUtills/exceptionHandler'
setJSExceptionHandler(errorAlert, true);


class Splash extends Component {

  constructor() {
    super();
    this.state = {
      input: "",
      isVisible: false
    }
  }


  componentDidMount() {

    DeviceInfo.getIPAddress().then(ip => {

      
      deviceMatrix = {
        "name": "app",
        "device_type": "mobile",
        "model": DeviceInfo.getModel(),
        "ip": ip,
        "version": DeviceInfo.getVersion(),
        "OS": DeviceInfo.getSystemVersion(),
        "device_id": DeviceInfo.getUniqueID(),
        "platform": DeviceInfo.getSystemName()
      };
      this.props.setDeviceMatrixValue(deviceMatrix);

      fetch(CONFIG.BASE_URL + 'auth/login/v1', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "login_name": "gomzyl.srk",
          "password": "gomzyl.srk1",
          "app_name": "solitaire",
          "org_name": "srkexports",
          "app_code": "1",
          "device_details": this.props.deviceMatrix
        })
      })
        .then((response) => response.json())
        .then((responseJson) => {

          this.props.setSessionValue(responseJson);
        })
        .catch((error) => {
          alert(error);

        });

    });
  }


  checkLogin = () => {
    this.setState({ isVisible: true })
    
   
    const requestParams = {
      "login_name": "gomzyl.srk",
      "password": "gomzyl.srk1",
      "app_name": "solitaire",
      "org_name": "srkexports",
      "app_code": "13",
      "device_details": {
        "name": "Chrome",
        "model": "",
        "device_type": "android",
        "version": "56",
        "ip": "103.254.246.50"
      }
    }

    

    ApiUtils.AxiosHelpers.executePostURL(StaticUtils.API_LOGIN, requestParams).then(
      (data) => {
        alert(data);
        setTimeout(() => {
          this.setState({ isVisible: false })
        }, 3000)

     //   this.setState({ isVisible: false })
        
      }

    ).catch((error) => {
      alert(error);
      setTimeout(() => {
        this.setState({ isVisible: false })
      }, 3000)
      console.log(error);
    });

    //throw new Error('THIS IS A CUSTOM ERROR');
  }



  render() {

    return (
      <View style={styles.container}>
       <OfflineNotice />
        <ProgressBar visible={this.state.isVisible} />
       
        <Text>{I18n.t(['label_username', 'entity_value'])}</Text>

        <Text style={styles.text}>
          API_URL={CONFIG.BASE_URL}
        </Text>

        <TouchableHighlight
          underlayColor="#ffa012"
          style={styles.button}
          onPress={() => this.checkLogin()}
        >
          <Text style={styles.buttonText}>Set Redux</Text>
        </TouchableHighlight>
        <TouchableHighlight
          underlayColor="#ffa012"
          style={styles.button}
          onPress={() => this.props.navigation.navigate('Login')}
        >
          <Text style={styles.buttonText}>Go Next</Text>
        </TouchableHighlight>
       
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    backgroundColor: '#ff9900',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 12,
    borderRadius: 3,
    padding: 8
  },
  buttonText: {
    color: 'white',
  }
});
export default helpers.connect(Splash);
