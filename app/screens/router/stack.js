
import { createStackNavigator } from 'react-navigation';

import Splash from '../splash/splash'
import Login from '../user/login'
import Test1 from '../user/test1'
import Test2 from '../user/test2'


 

export default st= createStackNavigator({
  Splash: {
    screen: Splash
  },
  Login: {
    screen: Login
  },
  Test1: {
    screen: Test1
  },
  Test2: {
    screen: Test2
  }
});


