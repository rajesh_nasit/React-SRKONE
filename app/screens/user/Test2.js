import React from 'react';
import { ScrollView } from 'react-native';
import helpers from '../../redux/connectors';


import {
  Text,
  View

} from 'react-native';

class Test2 extends React.Component {

  static navigationOptions = {
    title: 'Screen 2'
  };
  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View >
        <ScrollView>
          <View>
            <Text>Device Metrix: {JSON.stringify(this.props.deviceMatrix)}</Text>
            <Text>Login Metrix: {JSON.stringify(this.props.session)}</Text>

          </View></ScrollView>
      </View>
    )
  }
}



export default helpers.connect(Test2);
