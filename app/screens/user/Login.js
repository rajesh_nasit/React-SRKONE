
import React from 'react';
import { ScrollView } from 'react-native';
import helpers from '../../redux/connectors';


import {
  Text,
  View, TouchableHighlight, StyleSheet

} from 'react-native';

class Login extends React.Component {

  static navigationOptions = {
    title: 'Screen 0'
  };
  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View style={styles.container} >
        <ScrollView >
          <View>
            <TouchableHighlight
              underlayColor="#ffa012"
              style={styles.button}
              onPress={() => this.props.navigation.navigate('Test1')}
            >
              <Text style={styles.buttonText}>Go Next</Text>
            </TouchableHighlight>
            <Text>Device Metrix: {JSON.stringify(this.props.deviceMatrix)}</Text>
            <Text>Login Metrix: {JSON.stringify(this.props.session)}</Text>

          </View></ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    backgroundColor: '#ff9900',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 12,
    borderRadius: 3,
    padding: 8
  },
  buttonText: {
    color: 'white',
  }
});

export default helpers.connect(Login);
