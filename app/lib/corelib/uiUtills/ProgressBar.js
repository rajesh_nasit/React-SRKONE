import React from 'react';
import { Modal, View, Text, ActivityIndicator, StyleSheet } from 'react-native';

export default class ProgressBar extends React.Component {

  render() {
    const { visible } = this.props;
    return (
      <Modal onRequestClose={() => null} visible={visible} transparent={true}
        style={styles.modal}>
        <View style={styles.view1}>
          <View style={styles.view2}>
            <Text style={styles.text}>Loading</Text>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  view1: {
    alignSelf: 'center',
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    backgroundColor:'#00000090',
    width:'100%'
  },
  view2: {
    borderRadius: 10,
    width: 150,
    height: 150,
    backgroundColor: 'white',
    justifyContent: 'center'
  },
  text: {
    fontSize: 20,
    fontWeight: '200',
    alignSelf: 'center'
  }
})