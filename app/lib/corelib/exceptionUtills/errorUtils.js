var ErrorUtils = {  
    checkStatus: function(response) {
      if (response.status >= 200 && response.status < 300) {
        console.log(response.data.code);
        return response;
      } else {
        let error = new Error(response.statusText);
        error.response = response;
        throw error;
      }
    }
  };
  export { ErrorUtils as default }; 