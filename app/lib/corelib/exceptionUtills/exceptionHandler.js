import {Alert} from 'react-native'

export default errorAlert=(e,isFatal)=>{
  
    if (isFatal) {
      Alert.alert(
        'Unexpected error occurred',
         `
         Error: ${(isFatal) ? 'Fatal:' : ''} ${e.name} ${e.message}
  
         We have reported this to our team ! Please close the app and start again!
         `,
       [{
         text: 'Close'
       }]
     )
    } else {
      console.log(e); // So that we can see it in the ADB logs in case of Android if needed
  };
}