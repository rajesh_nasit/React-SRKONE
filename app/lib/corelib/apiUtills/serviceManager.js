import axios from 'axios';
import ErrorUtils from '../exceptionUtills/errorUtils'

export const AxiosHelpers = {
    executeGetURL: function () {

    },
    executePostURL: function (apiUrl,requestParams) {
      
        var res = axios.post(apiUrl,requestParams)
            .then(ErrorUtils.checkStatus)
            .then(function (response) {
                return response;
              })
        return res;
    },
}
