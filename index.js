import React from 'react'
import {
  AppRegistry,AsyncStorage
} from 'react-native'
console.disableYellowBox=true;
import { compose, createStore } from 'redux';
import { Provider } from 'react-redux'
import App from './app/screens/router/stack'
import { persistStore, autoRehydrate } from 'redux-persist';
import createEncryptor from 'redux-persist-transform-encrypt';
import rootReducer from './app/redux/reducers';

const store = createStore(
  rootReducer,
  undefined,
  compose(
    autoRehydrate()
  )
);

const encryptor = createEncryptor({
  secretKey: 'srkaycg'
});

persistStore(store, {
  storage: AsyncStorage,
  transforms: [
    encryptor
  ]
});

const RNRedux = () => (
  <Provider store={store}>
    <App />
  </Provider>
)

AppRegistry.registerComponent('SRKONE', () => RNRedux)
